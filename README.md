# Railway

[![Matrix](https://img.shields.io/badge/Matrix-Join-brightgreen)](https://matrix.to/#/%23railwayapp:matrix.org)

Railway lets you look up travel information for many different railways, all without needing to navigate through different websites.

## Screenshots

![](/data/screenshots/overview.png)

## Installation

<table>
  <tr>
    <td>Flatpak</td>
    <td>
      <a href='https://flathub.org/apps/details/de.schmidhuberj.DieBahn'><img width='130' alt='Download on Flathub' src='https://flathub.org/assets/badges/flathub-badge-en.png'/></a>
    </td>
  </tr>
  <tr>
    <td>Arch Linux (AUR)</td>
    <td>[diebahn](https://aur.archlinux.org/packages/diebahn)</td>
  </tr>
</table>

## Features

- Search for journeys
- View the details of a journey including departure, arrivals, delays, platforms
- Adaptive for small screens
- Bookmark a search or a journey
- Show more information like prices
- Many different search profiles, e.g
    - DB
    - ÖBB
    - BART
    - ... (many more). For a full list of supported profiles, see [hafas-rs](https://gitlab.com/schmiddi-on-mobile/hafas-rs#profiles) which is used to query the application data.

## Code of Conduct

This project follows [GNOME's Code of Conduct](https://wiki.gnome.org/Foundation/CodeOfConduct).

## Donate

If you like this project and have some spare Monero left, consider donating to my Monero address:

Monero:
```
82pRFY4iUjVUWm48ykaTKbjYeDksdMunWPHbrDbTmyKF7PWAxNX8FXM7G6B1n4NFvHfr3ztEg411A2gCjJjNJ8PtEnmcehf
```
